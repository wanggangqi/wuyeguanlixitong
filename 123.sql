-- MySQL dump 10.13  Distrib 5.1.30, for Win64 (unknown)
--
-- Host: 127.0.0.1    Database: test
-- ------------------------------------------------------
-- Server version	5.1.30-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `announce`
--

DROP TABLE IF EXISTS `announce`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `announce` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `sdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `announce`
--

LOCK TABLES `announce` WRITE;
/*!40000 ALTER TABLE `announce` DISABLE KEYS */;
INSERT INTO `announce` VALUES (2,'旅游公告','<p>2017年4月8号，大家一起去旅游，报名电话213234343</p>','2018-01-25 18:54:02'),(3,'下雨通知','<p>快下雨了，大家小心<img src=\"http://img.baidu.com/hi/jx2/j_0011.gif\"/></p>','2017-04-25 19:03:03'),(4,'下雪公告','<p>下雪路滑，大家小心<img src=\"http://img.baidu.com/hi/jx2/j_0016.gif\"/></p>','2017-04-25 19:03:47'),(5,'下雪公告','<p>可能会下雪，大家小心<img src=\"http://img.baidu.com/hi/jx2/j_0022.gif\"/></p>','2017-04-27 18:42:24'),(6,'修路公告','<p>正在修路，大家绕行<img src=\"http://img.baidu.com/hi/jx2/j_0024.gif\"/></p>','2017-04-27 18:43:07'),(7,'好吧还需要一条','<p>测试分页</p>','2017-04-27 18:43:31');
/*!40000 ALTER TABLE `announce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `charge`
--

DROP TABLE IF EXISTS `charge`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `charge` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `houseid` varchar(30) DEFAULT NULL,
  `month` varchar(30) DEFAULT NULL,
  `water` double(10,2) DEFAULT NULL,
  `electirc` double(10,2) DEFAULT NULL,
  `gas` double(10,2) DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `rname` varchar(20) DEFAULT NULL,
  `means` int(10) DEFAULT NULL,
  `cdate` date DEFAULT NULL,
  `wuye` double(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chargehouseid` (`houseid`),
  CONSTRAINT `chargehouseid` FOREIGN KEY (`houseid`) REFERENCES `house` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `charge`
--

LOCK TABLES `charge` WRITE;
/*!40000 ALTER TABLE `charge` DISABLE KEYS */;
INSERT INTO `charge` VALUES (29,'323232','2017-04',24.16,30.98,35.38,1,'王钢旗',1,'2017-04-27',7.04),(30,'10#101','2017-04',24.16,30.98,35.38,0,NULL,NULL,NULL,7.04),(32,'1#101','2017-04',6.64,7.74,8.84,0,NULL,NULL,NULL,7.04),(33,'2#301','2017-04',6.64,7.74,8.84,0,NULL,NULL,NULL,7.04),(34,'2#401','2017-04',6.64,7.74,8.84,0,NULL,NULL,NULL,7.04),(35,'2#402','2017-04',6.64,7.74,8.84,0,NULL,NULL,NULL,7.04),(36,'2#403','2017-04',6.64,7.74,8.84,0,NULL,NULL,NULL,7.04),(37,'2#404','2017-04',6.64,7.74,8.84,0,NULL,NULL,NULL,7.04),(38,'2#405','2017-04',6.64,7.74,8.84,0,NULL,NULL,NULL,7.04);
/*!40000 ALTER TABLE `charge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `electric`
--

DROP TABLE IF EXISTS `electric`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `electric` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `houseid` varchar(30) DEFAULT NULL,
  `floorid` varchar(30) DEFAULT NULL,
  `electric` double(10,2) DEFAULT NULL,
  `month` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `electrichouseid` (`houseid`),
  CONSTRAINT `electrichouseid` FOREIGN KEY (`houseid`) REFERENCES `house` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `electric`
--

LOCK TABLES `electric` WRITE;
/*!40000 ALTER TABLE `electric` DISABLE KEYS */;
INSERT INTO `electric` VALUES (22,'323232','10#',4.40,'2017-04'),(23,'10#101','10#',4.40,'2017-04'),(24,'1#101','2#',1.10,'2017-04'),(25,'2#301','2#',1.10,'2017-04'),(26,'2#401','2#',1.10,'2017-04'),(27,'2#402','2#',1.10,'2017-04'),(28,'2#403','2#',1.10,'2017-04'),(29,'2#404','2#',1.10,'2017-04'),(30,'2#405','2#',1.10,'2017-04');
/*!40000 ALTER TABLE `electric` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gas`
--

DROP TABLE IF EXISTS `gas`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `gas` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `houseid` varchar(30) DEFAULT NULL,
  `floorid` varchar(30) DEFAULT NULL,
  `gas` double(10,2) DEFAULT NULL,
  `month` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gashouseid` (`houseid`),
  CONSTRAINT `gashouseid` FOREIGN KEY (`houseid`) REFERENCES `house` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `gas`
--

LOCK TABLES `gas` WRITE;
/*!40000 ALTER TABLE `gas` DISABLE KEYS */;
INSERT INTO `gas` VALUES (22,'323232','10#',4.40,'2017-04'),(23,'10#101','10#',4.40,'2017-04'),(24,'1#101','2#',1.10,'2017-04'),(25,'2#301','2#',1.10,'2017-04'),(26,'2#401','2#',1.10,'2017-04'),(27,'2#402','2#',1.10,'2017-04'),(28,'2#403','2#',1.10,'2017-04'),(29,'2#404','2#',1.10,'2017-04'),(30,'2#405','2#',1.10,'2017-04');
/*!40000 ALTER TABLE `gas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hire`
--

DROP TABLE IF EXISTS `hire`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `hire` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `hname` varchar(30) DEFAULT NULL,
  `houseid` varchar(30) DEFAULT NULL,
  `hphone` varchar(30) DEFAULT NULL,
  `hcharge` double(10,2) DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `lease` int(10) DEFAULT NULL,
  `rname` varchar(20) DEFAULT NULL,
  `tdate` date DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `houseid` (`houseid`),
  CONSTRAINT `houseid` FOREIGN KEY (`houseid`) REFERENCES `house` (`account`),
  CONSTRAINT `pid` FOREIGN KEY (`pid`) REFERENCES `parkingspace` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `hire`
--

LOCK TABLES `hire` WRITE;
/*!40000 ALTER TABLE `hire` DISABLE KEYS */;
INSERT INTO `hire` VALUES (8,2,'21212','323232','122334',12.30,'2017-04-18',6,'32','2017-04-19','32'),(9,123,'12213232','323232','32324343',12.23,'2015-01-04',1,'王钢旗','2017-04-25','2132,上次受理人是212132'),(10,123,'王宝强','1#101','12324343454',12.23,'2016-05-05',1,'王钢旗','2017-04-27','还行');
/*!40000 ALTER TABLE `hire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `house`
--

DROP TABLE IF EXISTS `house`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `house` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `account` varchar(30) NOT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `floorid` varchar(30) DEFAULT NULL,
  `cellid` varchar(30) DEFAULT NULL,
  `bdate` varchar(30) DEFAULT NULL,
  `shape` varchar(20) DEFAULT NULL,
  `area` double(10,2) DEFAULT NULL,
  `owner` varchar(20) DEFAULT NULL,
  `population` int(10) DEFAULT NULL,
  `ownerphone` varchar(18) DEFAULT NULL,
  `mdate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `account` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `house`
--

LOCK TABLES `house` WRITE;
/*!40000 ALTER TABLE `house` DISABLE KEYS */;
INSERT INTO `house` VALUES (6,'323232','','10#','101','90年代','啥么特',103.32,'张三三三',10,'13671270097','2015-04-13'),(7,'1#101','面朝大海，春暖花开','2#','102','2002','杀马特风格',100.00,'王宝强',6,'12324343454','2017-04-18'),(8,'2#301','21','2#','301','2132','80年代',122.21,'财大气粗',6,'13671270097','2017-04-24'),(9,'2#401','非常棒','2#','401','21','fds',21.21,'张三',5,'12344555','2017-04-06'),(10,'2#402','还行','2#','402','21','21',212.10,'张思',7,'24343455','2017-04-11'),(11,'2#403','21','2#','403','21','21',32.32,'张武',32,'213324343','2017-04-12'),(12,'2#404','43','2#','404','323','21',32.00,'张柳',32,'21324343','2017-04-13'),(13,'2#405','3232','2#','','21','32',3232.00,'张器',23,'32324334','2017-04-06'),(14,'10#101','位于地中海地段，嘎嘎棒','10#','101','30年代','杀马特风格',130.00,'梵蒂冈',109,'1234567891','2017-04-07');
/*!40000 ALTER TABLE `house` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `message` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `houseid` varchar(30) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `isread` int(10) unsigned DEFAULT '0',
  `sdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `messagehouseid` (`houseid`),
  CONSTRAINT `messagehouseid` FOREIGN KEY (`houseid`) REFERENCES `house` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (2,'1#101','水表缴费通知单','王宝强您好,您从2017-04-21到2017-04-21进行的物业修理的费用(维修负责人),材料费:12.12元维修费:12.12元还没有缴费,请务必尽快进行缴费',1,'2017-04-21 14:10:47'),(3,'1#101','物业缴费通知单','王宝强您好,您2017-04的物业费还没有缴,请务必尽快进行缴费',1,'2017-04-25 11:52:34'),(4,'323232','电表缴费通知单','张三三您好,您从2017-04-27到2017-04-28进行的物业修理的费用(维修负责人),材料费:11.1元维修费:11.1元还没有缴费,请务必尽快进行缴费',0,'2017-04-27 17:03:58'),(5,'323232','物业缴费通知单','张三三您好,您2017-04的物业费还没有缴,请务必尽快进行缴费',0,'2017-04-27 18:28:20');
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parkingspace`
--

DROP TABLE IF EXISTS `parkingspace`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `parkingspace` (
  `id` int(10) NOT NULL,
  `location` varchar(40) DEFAULT NULL,
  `length` double(10,2) DEFAULT NULL,
  `width` double(10,2) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `maxyear` int(10) DEFAULT NULL,
  `charge` double(10,2) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `parkingspace`
--

LOCK TABLES `parkingspace` WRITE;
/*!40000 ALTER TABLE `parkingspace` DISABLE KEYS */;
INSERT INTO `parkingspace` VALUES (2,'2#-3#',32.30,23.40,1,10,12.30,'23'),(3,'2#-3#',132.00,12.00,0,12,2132.00,'123243'),(21,'2#-3#',32.00,3.00,0,32,32.00,NULL),(123,'1#-2#',5.50,4.00,2,10,12.23,'位于中央位置'),(131,'10#-11#',11.00,11.00,0,10,10.00,'比较小'),(2121,'1#-2#',2121.00,2121.00,0,21,212.00,NULL),(10101,'10#-11#',13.12,12.13,0,10,10.11,'车位比较大');
/*!40000 ALTER TABLE `parkingspace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price`
--

DROP TABLE IF EXISTS `price`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `price` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `month` varchar(30) DEFAULT NULL,
  `waterprice` double(10,2) DEFAULT NULL,
  `electricprice` double(10,2) DEFAULT NULL,
  `gasprice` double(10,2) DEFAULT NULL,
  `wuyeprice` double(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `price`
--

LOCK TABLES `price` WRITE;
/*!40000 ALTER TABLE `price` DISABLE KEYS */;
INSERT INTO `price` VALUES (2,'2017-05',24.12,25.12,26.12,27.12),(3,'2017-04',6.04,7.04,8.04,7.04),(7,'2017-06',100.00,100.00,100.00,100.00);
/*!40000 ALTER TABLE `price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repair`
--

DROP TABLE IF EXISTS `repair`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `repair` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `houseid` varchar(30) DEFAULT NULL,
  `category` varchar(30) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `reportdate` date DEFAULT NULL,
  `means` int(10) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `repairdate` date DEFAULT NULL,
  `principal` varchar(30) DEFAULT NULL,
  `material` double(10,2) DEFAULT NULL,
  `upkeep` double(10,2) DEFAULT NULL,
  `completedate` date DEFAULT NULL,
  `paydate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `repairhouseid` (`houseid`),
  CONSTRAINT `repairhouseid` FOREIGN KEY (`houseid`) REFERENCES `house` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `repair`
--

LOCK TABLES `repair` WRITE;
/*!40000 ALTER TABLE `repair` DISABLE KEYS */;
INSERT INTO `repair` VALUES (1,'323232','水表','水表坏了','2017-02-09',1,2,'2017-04-12','李四',12.00,12.00,'2017-04-13','2017-04-20'),(2,'1#101','水表','水表坏了','2017-02-10',2,2,'2017-04-21','王钢旗',12.12,12.12,'2017-04-21','2017-04-20'),(3,'1#101','电线','水表坏了','2017-02-09',3,2,'2017-02-12','王钢旗',12.00,30.00,'2017-04-20','2017-04-20'),(4,'323232','燃气表','水表坏了','2017-02-09',1,3,'2017-02-12','王钢旗',12.00,30.00,'2017-04-20','2017-04-20'),(5,'323232','电表','电表坏了，我也不知道怎么回事','2017-04-26',3,2,'2017-04-27','王钢旗',11.10,11.10,'2017-04-28',NULL),(6,'323232','水管','不知道怎么回事漏水了','2017-04-27',3,0,NULL,NULL,NULL,NULL,NULL,NULL),(7,'323232','燃气管','不知道怎么回事漏气了','2017-04-27',3,0,NULL,NULL,NULL,NULL,NULL,NULL),(8,'323232','燃气表','不知道怎么回事，燃气表不走了','2017-04-27',3,0,NULL,NULL,NULL,NULL,NULL,NULL),(9,'323232','水表','水表坏了','2017-04-27',3,0,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `repair` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `account` varchar(30) NOT NULL,
  `pwd` varchar(30) DEFAULT '123',
  PRIMARY KEY (`id`),
  KEY `account` (`account`),
  CONSTRAINT `account` FOREIGN KEY (`account`) REFERENCES `house` (`account`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (5,'323232','123'),(6,'1#101','123'),(7,'2#301','123'),(8,'2#401','123'),(9,'2#402','123'),(10,'2#403','123'),(11,'2#404','123'),(12,'2#405','123'),(13,'10#101','123');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `water`
--

DROP TABLE IF EXISTS `water`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `water` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `houseid` varchar(30) DEFAULT NULL,
  `floorid` varchar(30) DEFAULT NULL,
  `water` double(10,2) DEFAULT NULL,
  `month` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `waterhouseid` (`houseid`),
  CONSTRAINT `waterhouseid` FOREIGN KEY (`houseid`) REFERENCES `house` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `water`
--

LOCK TABLES `water` WRITE;
/*!40000 ALTER TABLE `water` DISABLE KEYS */;
INSERT INTO `water` VALUES (46,'1#101','2#',1.10,'2017-07'),(47,'2#301','2#',1.10,'2017-07'),(48,'2#401','2#',1.10,'2017-07'),(49,'2#402','2#',1.10,'2017-07'),(50,'2#403','2#',1.10,'2017-07'),(51,'2#404','2#',1.10,'2017-07'),(52,'2#405','2#',1.10,'2017-07'),(53,'323232','10#',1.10,'2017-07'),(54,'10#101','10#',1.10,'2017-07'),(55,'323232','10#',4.00,'2017-04'),(56,'10#101','10#',4.00,'2017-04'),(59,'1#101','2#',1.10,'2017-04'),(60,'2#301','2#',1.10,'2017-04'),(61,'2#401','2#',1.10,'2017-04'),(62,'2#402','2#',1.10,'2017-04'),(63,'2#403','2#',1.10,'2017-04'),(64,'2#404','2#',1.10,'2017-04'),(65,'2#405','2#',1.10,'2017-04');
/*!40000 ALTER TABLE `water` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wuye`
--

DROP TABLE IF EXISTS `wuye`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `wuye` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `houseid` varchar(30) DEFAULT NULL,
  `month` varchar(30) DEFAULT NULL,
  `wuye` double(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wuyehouseid` (`houseid`),
  CONSTRAINT `wuyehouseid` FOREIGN KEY (`houseid`) REFERENCES `house` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `wuye`
--

LOCK TABLES `wuye` WRITE;
/*!40000 ALTER TABLE `wuye` DISABLE KEYS */;
/*!40000 ALTER TABLE `wuye` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-27 11:52:41
