CREATE PROCEDURE updatestatus()
BEGIN
#首先将所有车位都设置成未出租 即0，
update parkingspace set status = 0;
#选择出已经出租的更新为1，因为快过期的范围比这个小，所有放在后面更新
update parkingspace set status = 1 where id in(
select pid from hire where DATEDIFF( DATE_ADD(startdate,INTERVAL lease YEAR),NOW())>0);
#选择出快过期的，更新为2
update parkingspace set status = 2 where id in(
select pid from hire where DATEDIFF( DATE_ADD(startdate,INTERVAL lease YEAR),NOW()) BETWEEN 0 and 30);
end
